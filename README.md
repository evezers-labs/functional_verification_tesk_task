# functional_verification_tesk_task

[![PVS-Studio analyze](https://img.shields.io/badge/PVS--Studio%20Static%20Analysis-see%20report-blue)](https://gitlab.com/evezers-labs/functional_verification_tesk_task/-/jobs/artifacts/main/file/PVS-Studio/full-report/index.html?job=pvs-studio-analyzer)
[![pipeline status](https://gitlab.com/evezers-labs/functional_verification_tesk_task/badges/main/pipeline.svg)](https://gitlab.com/evezers-labs/functional_verification_tesk_task/-/pipelines/latest)
[![coverage report](https://gitlab.com/evezers-labs/functional_verification_tesk_task/badges/main/coverage.svg)](https://gitlab.com/evezers-labs/functional_verification_tesk_task/-/jobs/artifacts/main/file/bin/cmake-build-debug-coverage/coverage-report/report.html?job=test)

Build project:
```[
cmake -B bin/cmake-build-debug/
cd bin/cmake-build-debug/
make
```

Tests:
```
cmake -B bin/cmake-build-debug-coverage/ -DCMAKE_CXX_FLAGS=--coverage -DCMAKE_C_FLAGS=--coverage
cd bin/cmake-build-debug-coverage/
make
make test
mkdir coverage-report
gcovr --xml-pretty --exclude-unreachable-branches --print-summary -o coverage-report/coverage.xml --root ../.. --html coverage-report/report.html --html-details --exclude ../../bin/ --exclude ../../test/ --exclude ../../cmake-build-*
```

Valgrind:

```
valgrind bin/cmake-build-debug/test/functional_verification_tesk_task_test
```


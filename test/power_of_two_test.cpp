//    This is a personal academic project. Dear PVS-Studio, please check it.
//
//    PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <gtest/gtest.h>

#include "../src/power_of_two.h"


TEST(PowerOfTwoTest, MainTest){
    EXPECT_EQ(is_power_of_two(1), true);
    EXPECT_EQ(is_power_of_two(2), true);
    EXPECT_EQ(is_power_of_two(4), true);
    EXPECT_EQ(is_power_of_two(8), true);

    EXPECT_EQ(is_power_of_two(-4), false);
    EXPECT_EQ(is_power_of_two(-2), false);
    EXPECT_EQ(is_power_of_two(-1), false);

    EXPECT_EQ(is_power_of_two(0), false);
    EXPECT_EQ(is_power_of_two(3), false);
    EXPECT_EQ(is_power_of_two(5), false);
    EXPECT_EQ(is_power_of_two(7), false);
    EXPECT_EQ(is_power_of_two(14), false);
    EXPECT_EQ(is_power_of_two(15), false);
    EXPECT_EQ(is_power_of_two(17), false);

    EXPECT_EQ(is_power_of_two(-3), false);
    EXPECT_EQ(is_power_of_two(-5), false);
    EXPECT_EQ(is_power_of_two(-7), false);
    EXPECT_EQ(is_power_of_two(-14), false);
    EXPECT_EQ(is_power_of_two(-15), false);
    EXPECT_EQ(is_power_of_two(-17), false);

    //EXPECT_EQ(is_power_of_two(1.1), true); // power_of_two.h:19:24: error: static assertion failed: Integral required.
}

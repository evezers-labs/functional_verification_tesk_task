//    This is a personal academic project. Dear PVS-Studio, please check it.
//
//    PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <gtest/gtest.h>

#include "../src/BitVector.h"


TEST(BitVectorTest, ConstructorTest) {
    uint32_t bits[2] = {0xDEADBEEF, 0xBABE};

    BitVector bit_vector(48, bits);

    EXPECT_STREQ(bit_vector.toString().c_str(), "babedeadbeef");
    EXPECT_EQ(bit_vector.getBitness(), 48);


    BitVector bit_vector2(46, bits);

    EXPECT_STREQ(bit_vector2.toString().c_str(), "3abedeadbeef");
    EXPECT_EQ(bit_vector2.getBitness(), 46);

    uint32_t bits1[2] = {0x76543210, 0xBA98};

    BitVector bit_vector1(46, bits1);

    EXPECT_STREQ(bit_vector1.toString().c_str(), "3a9876543210");



    uint8_t bits8[6] = {0xEF, 0xBE, 0xAD, 0xDE, 0xBE, 0xBA};
    BitVector bit_vector8(42, bits8);


    EXPECT_STREQ(bit_vector8.toString().c_str(), "2bedeadbeef");
}

TEST(BitVectorTest, SetBitVectorPaddedTest) {
    uint32_t bits[2] = {0xDEADBEEF, 0xBABE};
    BitVector bit_vector(48, bits);

    uint32_t bits1[2] = {0x76543210, 0xBA98};
    BitVector bit_vector1(46, bits1);

    bit_vector.setBit(16, bit_vector1);

    EXPECT_STREQ(bit_vector.toString().c_str(), "3a9876543210beef");

    EXPECT_EQ(bit_vector.getBitness(), 62);
}

TEST(BitVectorTest, SetBitVectorUnpaddedTest) {
    uint32_t bits[2] = {0xDEADBEEF, 0xBABE};
    BitVector bit_vector(32, bits);

    uint32_t bits1[2] = {0x76543210, 0xBA98};
    BitVector bit_vector1(33, bits1);

    EXPECT_STREQ(bit_vector.toString().c_str(), "deadbeef");
    EXPECT_STREQ(bit_vector1.toString().c_str(), "76543210");

    bit_vector.setBit(8, bit_vector1);

    EXPECT_STREQ(bit_vector.toString().c_str(), "76543210ef");
}

TEST(BitVectorTest, ToString) {
    uint32_t bits[2] = {0xD0000EEF, 0xBABE};
    BitVector bit_vector(48, bits);

    uint32_t bits2[2] = {0xDE000EEF, 0xBABE};
    BitVector bit_vector2(48, bits2);

    uint32_t bits1[2] = {0xDEADBEEF, 0x00BE};
    BitVector bit_vector1(48, bits1);

    uint32_t bits3[2] = {0xDEADBEEF, 0x0ABE};
    BitVector bit_vector3(48, bits3);

    uint32_t bits4[2] = {0x76543210, 0xBA98};
    BitVector bit_vector4(46, bits4);

    uint32_t bits5[2] = {0x76543200, 0xBA98};
    BitVector bit_vector5(46, bits5);

    EXPECT_STREQ(bit_vector.toString().c_str(), "babed0000eef");
    EXPECT_STREQ(bit_vector1.toString().c_str(), "bedeadbeef");
    EXPECT_STREQ(bit_vector2.toString().c_str(), "babede000eef");
    EXPECT_STREQ(bit_vector3.toString().c_str(), "abedeadbeef");
    EXPECT_STREQ(bit_vector4.toString().c_str(), "3a9876543210");
    EXPECT_STREQ(bit_vector5.toString().c_str(), "3a9876543200");
}

TEST(BitVectorTest, SetBitTest) {
    uint32_t bits1[2] = {0x76543210, 0xBA98};
    BitVector bit_vector1(46, bits1);

    EXPECT_STREQ(bit_vector1.toString().c_str(), "3a9876543210");

    bit_vector1.setBit(0, 4, 0xC);

    EXPECT_STREQ(bit_vector1.toString().c_str(), "3a987654321c");

    bit_vector1.setBit(4, 4, 0xB);

    EXPECT_STREQ(bit_vector1.toString().c_str(), "3a98765432bc");

    EXPECT_EQ(bit_vector1.getBitness(), 46);

    uint8_t val8 = 0;

    EXPECT_THROW(bit_vector1.setBit(0, 32, val8), std::out_of_range);
    EXPECT_NO_THROW(bit_vector1.setBit(0, 0, val8));

    EXPECT_STREQ(bit_vector1.toString().c_str(), "3a98765432bc");

    uint32_t bits[2] = {0xDEADBEEF, 0xBABE};

    BitVector bit_vector(48, bits);


    uint32_t val16 = 0xBABEDADA;
    bit_vector.setBit(0, 20, val16);
    EXPECT_STREQ(bit_vector.toString().c_str(), "babedeaedada");

    val16 = 0xBAB12345;

    bit_vector.setBit(4, 20, val16);
    EXPECT_STREQ(bit_vector.toString().c_str(), "babede12345a");
    BitVector empty_vector(0, &val8);

    bit_vector.setBit(0, empty_vector);
    EXPECT_STREQ(empty_vector.toString().c_str(), "");
}

TEST(BitVectorTest, GetBitField) {
    uint32_t bits[2] = {0xDEADBEEF, 0xBABE};
    BitVector bit_vector(48, bits);


    auto val16 = bit_vector.getBitfield<uint16_t>(0, 8);
    auto val32 = bit_vector.getBitfield<uint32_t>(4, 16);
    auto val64 = bit_vector.getBitfield<uint64_t>(8, 40);

    EXPECT_EQ(val16, 0xEF);
    EXPECT_EQ(val32, 0xDBEE);
    EXPECT_EQ(val64, 0xBABEDEADBE);

    EXPECT_THROW(bit_vector.getBitfield<uint16_t>(0, 23), std::overflow_error);
    EXPECT_THROW(bit_vector.getBitfield<uint64_t>(8, 48), std::out_of_range);
}

//    This is a personal academic project. Dear PVS-Studio, please check it.
//
//    PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <gtest/gtest.h>

#include "../src/shuffled_sample.h"

#include <random>
#include <memory>

TEST(ShuffledSampleTest, AscendingSequence) {
    std::mt19937 gen {42};

    std::pmr::vector<int> nums;

    for (int i = 0; i< 10; ++i) {
        nums.push_back(i);
    }

    auto newnums = shuffled_sample(nums, 5, gen);


    for (const auto num: newnums) {
        std::cout << std::to_string(num) << " " ;
    }

    std::cout << std::endl;


    ASSERT_EQ(newnums.size(), 5);
    ASSERT_EQ(newnums[0], 3);
    ASSERT_EQ(newnums[1], 7);
    ASSERT_EQ(newnums[2], 9);
    ASSERT_EQ(newnums[3], 1);
    ASSERT_EQ(newnums[4], 5);
}


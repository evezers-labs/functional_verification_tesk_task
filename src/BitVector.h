//    This is a personal academic project. Dear PVS-Studio, please check it.
//
//    PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#ifndef BITVECTOR_H
#define BITVECTOR_H
#include <cstdint>
#include <ranges>
#include <string>
#include <vector>

/**
* Реализовать класс битового вектора произвольной разрядности.
*
* Разрядное (битовое) поле битового вектора определяется номерами позиций
* младшего и старшего разряда поля.
*
* Для хранения одного разряда битвектора следует использовать один бит памяти.
 */
class BitVector {
    typedef uint8_t container_type;
    static constexpr size_t container_type_bitness = sizeof(container_type) * 8;

    size_t current_bitness;
    std::vector<container_type> container;

    void assignShifted(const size_t idx, const size_t offset, const container_type num) {
        container[idx] &= (1 << offset) - 1;
        container[idx] |= (num << offset);
    }

    void assignUnshifted(const size_t idx, const size_t offset, container_type num) {
        container[idx] &= ~((1 << offset) - 1);
        num &= (1 << offset) - 1;
        container[idx] |= num;
    }

    /**
    * Установить битовое поле:
    * вариант для установки из массива интегрального типа.
    *
    * Недоступен извне, так как не описан в исходном интерфейсе.
    *
     * @param offset смещение
     * @param bitness разрядность
     * @param bits массив переменных интегрального типа
     * @return новый битовый вектор
     */
    template <class T>
    BitVector &setBits(size_t offset, const size_t bitness, const T *bits) {
        if (!bitness) {
            return *this;
        }

        static_assert(std::is_integral_v<T>, "Integral required.");

        constexpr size_t field_size = sizeof(T) * 8;

        const size_t full_fields_count = bitness / field_size;
        const size_t last_field_bitness = bitness % field_size;

        const size_t full_bitness = offset + bitness;
        const size_t new_bitness = std::max(current_bitness, full_bitness);

        const size_t vector_size = new_bitness / container_type_bitness +
                                    (new_bitness % container_type_bitness ? 1 : 0);

        container.resize(vector_size);

        for (int i = 0; i < full_fields_count; ++i) {
            setBit(offset + i * field_size, field_size, bits[i]);
        }

        if (last_field_bitness) {
            setBit(offset + full_fields_count * field_size, last_field_bitness, bits[full_fields_count]);
        }

        return *this;
    }
public:
    /**
    * Конструктор:
    * нулевой разряд нулевого элемента массива соответствует нулевому разряду битвектора;
     * @param bitness разрядность
     * @param bits массив переменных интегрального типа
     */
    template <class T>
    BitVector(const size_t bitness, const T bits[])
        : current_bitness(bitness) {
        setBits(0, bitness, bits);
    }

    /**
     * Получить разрядность;
     * @return разрядность
     */
    [[nodiscard]] size_t getBitness() const {
        return current_bitness;
    }

    /**
    * Установить битовое поле;
     * @param offset смещение
     * @param bit_field битвектор
     * @return новый битовый вектор
     */
    BitVector &setBit(const size_t offset, const BitVector &bit_field) {
        if (!bit_field.current_bitness) {
            return *this;
        }

        return setBits(offset, bit_field.current_bitness, bit_field.container.data());
    }

    /**
    * Установить битовое поле: отдельный вариант для битовых полей малой разрядности
    * (до 64 разрядов);
     * @param offset смещение
     * @param bitness разрядность
     * @param bit_field переменная интегрального типа
     * @return новый битовый вектор
     */
    template <class T>
    BitVector &setBit(const size_t offset, const size_t bitness, T bit_field) {
        if (!bitness) {
            return *this;
        }

        static_assert(std::is_integral_v<T>, "Integral required.");

        if (bitness > sizeof(T) * 8) {
            throw std::out_of_range("Bitness exceedes bit_field value size.");
        }

        size_t container_idx = offset / container_type_bitness;
        const size_t container_bit = offset % container_type_bitness;

        if (container_bit) {
            assignShifted(container_idx, container_bit, static_cast<container_type>(bit_field));
            bit_field >>= container_type_bitness - container_bit;
            container_idx++;
        }

        const size_t bit_ops = bitness - container_bit;

        const size_t field_count = bit_ops / container_type_bitness;
        const size_t field_bit = bit_ops % container_type_bitness;

        for (int i = 0; i < field_count; ++i) {
            container[container_idx + i] = static_cast<container_type>(bit_field);
            bit_field >>= container_type_bitness;
        }

        if (field_bit) {
            assignUnshifted(container_idx + field_count, field_bit, bit_field);
        }

        if (offset + bitness > current_bitness) {
            current_bitness = offset + bitness;
        }

        return *this;
    }

    /**
    * Получить битовое поле;
     * @param offset смещение
     * @param bitness разрядность
     * @return исходный битовый вектор
     */
    [[nodiscard]] BitVector getBitfieldVector(const size_t offset, const size_t bitness) const {
        if (bitness + offset > current_bitness) {
            throw std::out_of_range("Bitness and offset exceedes BitVector size.");
        }

        BitVector offsetBitfield = BitVector(0, container.data());

        offsetBitfield.setBits(offset, bitness, container.data());

        return offsetBitfield;
    }

    /**
    * Получить битовое поле: отдельный вариант для битовых полей малой разрядности
    * (до 64 разрядов);
     * @param offset смещение
     * @param bitness разрядность
     * @return переменную интегрального типа
     */
    template <class T>
    T getBitfield(const size_t offset, const size_t bitness) {
        static_assert(std::is_integral_v<T>, "Integral required.");
        constexpr size_t field_size = sizeof(T) * 8;

        if (bitness > field_size) {
            throw std::overflow_error("Bitness exceedes returning value size.");
        }

        if (bitness + offset > current_bitness) {
            throw std::out_of_range("Bitness and offset exceedes BitVector size.");
        }

        T val = 0;

        const size_t end_bit = offset + bitness;

        size_t container_idx = end_bit / container_type_bitness;
        const size_t container_bit = end_bit % container_type_bitness;

        auto tmp = container[container_idx] & (1 << container_bit) - 1;
        val |= tmp;
        container_idx--;

        const size_t bit_ops = bitness - container_bit;

        const size_t field_count = bit_ops / container_type_bitness;
        const size_t field_bit = bit_ops % container_type_bitness;

        for (int i = 0; i < field_count; ++i) {
            val <<= container_type_bitness;
            val |= container[container_idx - i];
        }

        if (field_bit) {
            val <<= container_type_bitness - field_bit;
            val |= container[container_idx - field_count] >> field_bit;
        }

        return val;
    }

    /**
     * Получить строковое представление в 16-ричном виде.
     * @return строковое представление в 16-ричном виде.
     */
    std::string toString() {
        std::ostringstream ss;

        ss << std::hex;

        bool significant = false;

        for (auto num : std::ranges::reverse_view(container))
        {
            if (significant) {
                ss << std::setfill('0') << std::setw(sizeof(container_type) * 2) ;
            }

            if (num || significant) {
                ss << +num;
            }

            if (num && !significant) {
                significant = true;
            }
        }

        return ss.str();
    }

};

#endif //BITVECTOR_H

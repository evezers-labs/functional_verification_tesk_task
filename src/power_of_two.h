//    This is a personal academic project. Dear PVS-Studio, please check it.
//
//    PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#ifndef POWER_OF_TWO_H
#define POWER_OF_TWO_H

/**
* Реализовать функцию от аргумента a интегрального типа, возвращающую
* признак того, что a является степенью двойки:
*
* f (a) == ((\exists i \in \N \cup {0}): a == 2^i).
*
 * @param a аргумент интегрального типа
 * @return признак того, что a является степенью двойки
 */
template <class T>
constexpr bool is_power_of_two(const T a) {
    static_assert(std::is_integral_v<T>, "Integral required.");
    return a and ((a & (a - 1)) == 0);
}

#endif //POWER_OF_TWO_H

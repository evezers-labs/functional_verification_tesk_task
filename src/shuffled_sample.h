//    This is a personal academic project. Dear PVS-Studio, please check it.
//
//    PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#ifndef SHUFFLED_SAMPLE_H
#define SHUFFLED_SAMPLE_H

#include <algorithm>
#include <iterator>
#include <set>
#include <vector>

/**
 * Реализовать функцию, для данного набора входных элементов (A)
 * возвращающую случайную выборку из n элементов (n <= size_of (A)).
 * Все возможные выборки должны быть равновероятны.
 * Элементы входного набора считать попарно различными.
 * @param A набор входных элементов
 * @param n количество элементов в выходном наборе
 * @param gen генератор псевдослучайных чисел
 * @return случайную выборку из n элементов
 */
template<class T, class Gen>
    requires std::uniform_random_bit_generator<std::remove_reference_t<Gen>>
std::pmr::vector<T> shuffled_sample(std::pmr::vector<T> &A, const int n, Gen&& gen){
    auto oldsize = std::size(A);

    std::uniform_int_distribution<> random_distribution(0, oldsize - 1);

    std::pmr::set<int> indices_set;

    std::pmr::vector<T> result_array;

    for (int i = 0; i < n; ++i) {
        bool is_already_inserted;
        int num;

        do {
            num = random_distribution(gen);
            is_already_inserted = indices_set.insert(num).second;
        } while (!is_already_inserted);

        result_array.push_back(A[num]);
    }

    return result_array;
}

#endif //SHUFFLED_SAMPLE_H
